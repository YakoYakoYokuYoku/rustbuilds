# Rust-for-Linux kernel builds

This document explains on how to build and test custom Linux kernels with Rust support.

## Prior requirements

- Kernel source files, obtained by a clone, either full or blobless (just make sure to fetch the target commit or tag), or by
  downloading a tarball.
- Rust support patches (explained in the next section).
- Build tools
  - Make (preferably GNU Make), Patch, BC, LibElf and Perl.
  - Clang (compiling Rust kernels with GCC is considered experimental).
  - Rust and Bindgen (with versions greater than or equal to the ones from the patchset).
  - Cpio, tar and one of xz or zstd.
  - Pahole for BTF debug information (one version of it that supports the `--lang_exclude` flag).
  - Distro packaging tools if a software package build is desired.
- Git in case of using clones.
- Initramfs utilities (e.g. Dracut, Booster, MkInitCpio, etc) for bare metal and VM testing.
- SystemD for the boot image (other boot loaders are supported).
- QEMU, Busybox and qcow2 images for QEMU testing.
- Alternative boot configurations are required to lifebuoy a failing one.

## Patching

To obtain the Rust support patches I advice one of these two options, to go to an archive of the Linux development mailing
list ([rust-for-linux][1] from the official archives is the best one out) and download the mbox file of the entire thread or
use the git fork from [Rust-for-Linux][2]. Also patches from the `patches` folder might be required for some Kernel configs.

This repository includes a Python script named `mboxcleaner` that is in charge of cleaning up and sorting mbox files while
leaving a proper file for patching and it also can be used to list all the messages from input. If a message is missing, the
same or other archives must be searched for it and it has to be appended to the mbox file (use the `-a` flag or append it
directly to the mbox file with two empty CRLF lines).

The patch series has to be tested in a dry run (via `patch --dry-run -Np1 -i Rust-support.patch`) before applying them to the
kernel sources to ensure that it can be done cleanly. If conflicts appear then blames should be used to identify missing
commits to later be downloaded.

## Configuring

With all patches present they have to be applied in a correct succession, first patches should be the ones from the previous
commits, then the Rust support patch and lastly the ones from `patches`. After that Kbuild needs to be configured with Clang
by setting the `LLVM` Make variable to `1`, doing it with `menuconfig` is my recommendation.

```
$ make menuconfig LLVM=1
```

Any old configuration should be backup first if desired. If `CONFIG_RUST` is off then check `menuconfig` to make sure version
requirements and conditions are met.

## Building

To build the kernel Make needs to be called with `LLVM=1` too and in subsequent invocations (including docs generation and
module installation, testing, between others) so then it uses LLVM/Clang tooling in the compilation of C sources.

```
$ make all LLVM=1
$ make htmldocs LLVM=1 # for HTML documentation
```

If compilation errors appear then it's advisable to look for commits that are required to be reverted in the
[Rust-for-Linux fork][2], omit blamed patches, rewrite them otherwise or add newer patches from upstream, the fork or locally
developed.

## Testing

Options for testing the built kernel and modules include QEMU, bare metal and KUnit. For all of these tests I recommend to
generate a custom Initramfs image using a generator of choice.

```
$ dracut -c custom.conf testing.img <kernel version>
$ booster -config custom.yaml -kernelVersion <kernel version> -output testing.img
$ mkinitcpio -c custom.conf -k <kernel version> -g testing.img
```

To perform tests in QEMU I encourage the usage of a custom qcow2 image with Busybox to make things easier.

```
$ mke2fs -t ext4 -d sysroot test.ext4 <filesystem size>
$ qemu-img convert test.ext4 test.qcow2
```

Launch QEMU with KVM enabled, 256M or more of memory (lower memory might not boot), the built kernel (`vmlinuz-linux`,
`vmlinuz` or `bzImage`), the Initramfs and qcow2 images. Optionally supply `-append` with kernel options if these weren't in
the Initramfs and `-bios path/to/edk2/OVMF.fd` to use UEFI.

```
$ qemu-system-x86_64 -enable-kvm -m 256 -kernel path/to/vmlinuz-linux -initrd testing.img -hda test.qcow2
```

To test directly in bare metal a boot loader entry needs to be added (consult the documentation of the chosen boot loader)
and another entry or USB live image has to be present for an emergency boot, then the testing hardware needs to be booted up.

The KUnit test suite can be run either on QEMU or bare metal. Follow the [KUnit section][3] at the official documentation.

[1]: https://lore.kernel.org/rust-for-linux
[2]: https://github.com/Rust-for-Linux/linux
[3]: https://www.kernel.org/doc/html/latest/dev-tools/kunit/index.html
